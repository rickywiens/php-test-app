<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class KegTest extends TestCase
{
    /**
     *
     * @return void
     */
    public function testGetBasic()
    {
        $response = $this->get('/');

        $response->assertStatus(200);
    }

    public function testGetOne()
    {
        $response = $this->get('/kegs');

        $response->assertStatus(200);
    }
}
