<?php

namespace App\Http\Controllers;

use App\Keg;
use Illuminate\Http\Request;

class KegController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json(["Keg" => ["name" => "myKeg"]]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Keg  $keg
     * @return \Illuminate\Http\Response
     */
    public function show(Keg $keg)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Keg  $keg
     * @return \Illuminate\Http\Response
     */
    public function edit(Keg $keg)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Keg  $keg
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Keg $keg)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Keg  $keg
     * @return \Illuminate\Http\Response
     */
    public function destroy(Keg $keg)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Keg  $keg
     * @return \Illuminate\Http\Response
     */
    public function drain(Keg $keg)
    {
        //
    }    
}
